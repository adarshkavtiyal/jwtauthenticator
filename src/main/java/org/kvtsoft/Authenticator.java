package org.kvtsoft;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;

public class Authenticator {

    public static JSONObject CreateJwt(String userId) {

        // The JWT signature algorithm to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DATE, 1);

        // Signing JWT with ApiKey/Secret key
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(System.getenv("jwtSecret"));
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        // JWT Claims
        JwtBuilder builder = Jwts.builder().setId(userId).setIssuedAt(now).setIssuer("email")
                .signWith(signatureAlgorithm, signingKey).setExpiration(cal.getTime());

        // Builds the JWT and serializes it to a compact, URL-safe string
        JSONObject response = new JSONObject();
        response.put("jwt", builder.compact());
        //response.put("role", "");
        response.put("message", "Logged in successfully !!");
        response.put("statusCode", 200);

        return response;
    }

    public static JSONObject verifyJwt(String jwt, String secret) {

        JSONObject response = new JSONObject();

        try {
            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(secret))
                    .parseClaimsJws(jwt).getBody();

            if (claims != null && !(claims.isEmpty())) {
                response.put("claims", claims);
                response.put("message", "valid");
                response.put("statusCode", 200);
            } else {
                response.put("message", "invalid");
                response.put("statusCode", 400);
            }
        } catch (Exception e) {
            response.put("message", e.getMessage());
            response.put("statusCode", 400);
        }

        return response;
    }
}
